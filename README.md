<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```
## Configure the project
```bash
$ cp .env.example .env
```

## Docker app
```bash
# The project uses docker to connect to the PostgreSQL database, you should have docker installed to run the application
$ docker compose up -d
```
This project is not using seeder data so the project guards are disabled.

## Running the app

```bash
$ npm run start:dev
```

## Test

```bash
$ npm run test -- --verbose
```

## If you want to see somethins more especific use
``` bash
$ npm run test:rol -- --verbose
# Just Rol
```
The answer:
``` bash
# The answer:
# PASS  src/rol/service/rol.service.spec.ts
#  RolService
#    √ Deberia de estar definido (18 ms)
#    create
#      √ Deberia de llamar al repositorio para guardar el nuevo Rol (6 ms)
#    findAll
#      √ Deberia de llamar al repositorio para listar los registros de los Roles (2 ms)
#    findOne
#      √ Deberia de llamar al repositorio para buscar a un registro del los Roles (2 ms)
#    update
#      √ Deberia de llamar al repositorio para actualizar un registro de Rol (2 ms)
#    remove
#      √ Deberia de llamar al repositorio para eliminar un registro de los Roles (3 ms)

# PASS  src/rol/controller/rol.controller.spec.ts
#  RolController
#    √ Deberia de estar definido (27 ms)
#    create
#      √ Debeia de llamar al servicio con los parametros esperados (5 ms)
#    findAll
#      √ DEberia llamar a listar con la paginacion de los datos (2 ms)
#    findOne
#      √ Deberia de llamar al servicio para buscar a solo un registro (2 ms)
#    update
#      √ Deberia de llamar al servicio para modificar un registro de Rol (2 ms)
#    remove
#      √ Deberia llamar al servicio para eliminar el Rol (3 ms)

# Test Suites: 2 passed, 2 total
# Tests:       12 passed, 12 total
# Snapshots:   0 total
# Time:        4.976 s, estimated 12 s
```
``` bash
$ npm run test:usuario -- --verbose
# Just Usuario
```
The answer:

``` bash
# FAIL  src/usuario/service/usuario.service.spec.ts
#  UsuarioService
#    create
#      √ Deberia de Agregar un nuevo usuario por el repositorio de Usuario (26 ms)
#      × Deberia de devolver un error si el usuario existe (29 ms)
#    findAll
#      √ Deberia de listar a los usuario por el repositorio de Usuario (2 ms)
#    findOne
#      √ Derberia de retornar un usuario por busqueda por ID por el repositorio de Usuario (2 ms)
#      × Deberia de retornar usuario no encontrado por ID (3 ms)
#    update
#      √ Deberia de actualizar un usuario por el repositorio de Usuario (2 ms)
#      × Deberia de devolver un error al quere actualizar un usuario (2 ms)
#    remove
#      √ Deberia de eliminar a un usuario (3 ms)
#      × Deberia de devolver error al no encontrar el usuario (4 ms)
#    verificarCuenta
#      √ Deberia de verificar el usuario y contraseña para validar el acceso (2 ms)
#      × Deberia de dar error al querer ingresar con creadenciales incorrectas (3 ms)
#    getPokemon
#      √ Deberia de mostrar informacion de la API (3 ms)
#      √ Deberia de mostrar error al no llegar a la API (52 ms)
#    getPokemonById
#      √ Deberia de devolver un pokemon por ID (3 ms)
#      √ Deberia de dar un error por buscar un Pokemon por ID (8 ms)
# PASS  src/usuario/controller/usuario.controller.spec.ts
#  UsuarioController
#    √ should be create a user (20 ms)

# Test Suites: 1 failed, 1 passed, 2 total
# Tests:       5 failed, 12 passed, 17 total
# Snapshots:   0 total
# Time:        5.298 s, estimated 6 s
# Ran all test suites matching /src\\usuario/i.
```
``` bash
$ npm run test:asignacion-rol -- --verbose
```
Answer:
``` bash
# PASS  src/asignacion-rol/service/asignacion-rol.service.spec.ts
#  AsignacionRolService
#    create
#      √ Deberia de crear una nueva asignacion (18 ms)
#    findAll
#      √ Deberia de devolver a las asiganciones Usuario Rol (3 ms)
#    findByUsuarioId
#      √ Deberia de devolver a los roles de un usuario especifico (2 ms)
#    remove
#      √ Deberia eliminar la asignacion por ID (2 ms)

# PASS  src/asignacion-rol/controller/asignacion-rol.controller.spec.ts
#  AsignacionRolController
#    √ should be create a new user with rol (17 ms)

# Test Suites: 2 passed, 2 total
# Tests:       5 passed, 5 total
# Snapshots:   0 total
# Time:        4.876 s, estimated 12 s

# Test Suites: 1 failed, 1 passed, 2 total
# Tests:       5 failed, 11 passed, 16 total
# Snapshots:   0 total
# Time:        4.965 s, estimated 12 s
```
## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Juan Carlos Vasquez] [juancarlosvasquezmacias@hotmail.com]

## License

Nest is [MIT licensed](LICENSE).
