export class Answer {
    message: string;
    status: boolean;
    data: Object;
    all: Object;
}