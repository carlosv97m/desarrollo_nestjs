import { IsNotEmpty, IsNumber } from "class-validator";

export class FindById {

    @IsNumber()
    @IsNotEmpty()
    id: number;
}