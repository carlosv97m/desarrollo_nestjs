import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Query } from '@nestjs/common';
import { AsignacionRolService } from '../service/asignacion-rol.service';
import { CreateAsignacionRolDto, GetIdAsignacionRolDto, GetByIdUsuario } from '../dto/index.dto';
import { RolesGuard, JwtStrategy, JwtauthGuard } from '../../auth/guards/index.guard'
import { ApiTags } from '@nestjs/swagger';
@ApiTags('Asignacion Cargo Controller')
// @UseGuards(RolesGuard, JwtStrategy, JwtauthGuard)
@Controller('asignacion-rol')
export class AsignacionRolController {
  constructor(private readonly asignacionRolService: AsignacionRolService) {}

  @Post()
  create(@Body() createAsignacionRolDto: CreateAsignacionRolDto) {
    return this.asignacionRolService.create(createAsignacionRolDto);
  }

  @Get()
  findAll() {
    return this.asignacionRolService.findAll();
  }

  @Get('/getAsginacionUsuarioById/:id')
  async findBtUsuarioId(@Param('id') id: number, @Body() getByIdUsuario: GetByIdUsuario ){
    const data = await this.asignacionRolService.findByUsuarioId(getByIdUsuario)
    console.log("encontrado:", {data})
    return data;
  }

  @Delete(':id')
  remove(@Query('id') id: GetIdAsignacionRolDto) {
    return this.asignacionRolService.remove( id );
  }
}
