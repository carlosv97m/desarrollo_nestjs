import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreateAsignacionRolDto } from './create-asignacion-rol.dto';
import { IsNotEmpty, IsNumber } from 'class-validator';

export class UpdateAsignacionRolDto extends PartialType(CreateAsignacionRolDto) {

    @ApiProperty({
        example: '1',
        required: true
    })
    @IsNumber()
    @IsNotEmpty()
    id: number

    @ApiProperty({
        example: '1',
        required: true
    })
    @IsNumber()
    @IsNotEmpty()
    usuario_id: number

    @ApiProperty({
        example: '1',
        required: true
    })
    @IsNumber()
    @IsNotEmpty()
    rol_id: number
}
