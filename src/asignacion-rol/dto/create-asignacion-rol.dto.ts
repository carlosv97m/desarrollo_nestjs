import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber } from "class-validator";

export class CreateAsignacionRolDto {
    @ApiProperty({
        example: '1',
        required: true
    })
    @IsNumber()
    @IsNotEmpty()
    usuario_id: number

    @ApiProperty({
        example: '1',
        required: true
    })
    @IsNumber()
    @IsNotEmpty()
    rol_id: number
}
