import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { Injectable, InternalServerErrorException, NotFoundException } from "@nestjs/common";
import { AsignacionRol, Rol, Usuario } from "../../entities/index";
import { CreateAsignacionRolDto, UpdateAsignacionRolDto, GetByIdUsuario, GetIdAsignacionRolDto } from '../dto/index.dto';
import { UsuarioConRolesDTO, AsignacionRolDTO, RolDTO } from '../interface/usuarioAsignacionRol.interface'
@Injectable()
export class AsignacionRepository {
    constructor(
        @InjectRepository(Usuario)
        private readonly usuarioRepository: Repository<Usuario>,
        @InjectRepository(Rol)
        private readonly rolRepository: Repository<Rol>,
        @InjectRepository(AsignacionRol)
        private readonly asignacionRolRepository: Repository<AsignacionRol>
    ) { }

    async crear(createAsignacionRolDto: CreateAsignacionRolDto) {
        try {
            const a_usuarios = await this.usuarioRepository.findOne({ where: { id: createAsignacionRolDto.usuario_id } })
            const a_roles = await this.rolRepository.findOne({ where: { id: createAsignacionRolDto.rol_id } })
            if ((!a_usuarios) && (!a_roles)) { throw new NotFoundException(`Error al registrar`) }
            const data = new AsignacionRol()
            data.rol = a_roles
            data.usuario = a_usuarios
            return this.asignacionRolRepository.save(data);
        } catch (error) {
            throw new InternalServerErrorException(error.message)
        }
    }
    async actaulizar(id: number, updateAsignacionRolDto: UpdateAsignacionRolDto) {
        try {
            const data = await this.asignacionRolRepository.findOneBy({ id });
            if (!data) {
                throw new NotFoundException('No se encontró la información');
            }

            const usuario = await this.usuarioRepository.findOneBy({ id: updateAsignacionRolDto.usuario_id });
            const rol = await this.rolRepository.findOneBy({ id: updateAsignacionRolDto.rol_id });

            if (!usuario || !rol) {
                throw new NotFoundException('Error al modificar');
            }

            data.usuario = usuario;
            data.rol = rol;

            return this.asignacionRolRepository.save(data);
        } catch (error) {
            throw new InternalServerErrorException(error.message);
        }
    }
    async listar() {
        try {
            const data = await this.asignacionRolRepository.find({
                relations: ['usuario', 'rol'],
                where: {
                    usuario: {
                        deleteAt: null
                    },
                    rol: {
                        deleteAt: null
                    },
                    deleteAt: null
                }
            });
            if (!data) { throw new NotFoundException("No se encontraron Datos") }
            return data;
        } catch (error) {
            throw new InternalServerErrorException(error.message)
        }
    }

    async listarByUsuario(getByIdUsuario: GetByIdUsuario): Promise<UsuarioConRolesDTO[]> {
        try {
            const { id } = getByIdUsuario;
            const usuariosConRoles: Usuario[] = await this.usuarioRepository.find({
                relations: ['asignacionesRoles', 'asignacionesRoles.rol'],
                where: {
                    id,
                    deleteAt: null,
                },
            });
            return usuariosConRoles.map(usuario => ({
                id: usuario.id,
                nombre: usuario.nombre,
                nombreUsuario: usuario.nombreUsuario,
                email: usuario.email,
                asignacionesRoles: usuario.asignacionesRoles.map(asignacion => ({
                    id: asignacion.id,
                    rol: {
                        id: asignacion.rol.id,
                        nombre: asignacion.rol.nombre,
                        descripcion: asignacion.rol.descripcion,
                    } as RolDTO,
                })) as AsignacionRolDTO[],
            })) as UsuarioConRolesDTO[];
        } catch (error) {
            throw new InternalServerErrorException(error.message);
        }
    }
    async remover_asignacion(getIdAsignacionRolDto: GetIdAsignacionRolDto) {
        try {
            const { id } = getIdAsignacionRolDto;
            const asignacionRol = await this.asignacionRolRepository.findOneBy({ id });
            if (!asignacionRol) {
                throw new Error(`La asignación de rol con ID ${id} no fue encontrada.`);
            }
            await this.asignacionRolRepository.delete(id);

        } catch (error) {
            throw new InternalServerErrorException(`Error al eliminar la asignación de rol: ${error.message}`);
        }
    }

}