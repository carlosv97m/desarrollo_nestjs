import { Injectable } from '@nestjs/common';
import { CreateAsignacionRolDto, GetIdAsignacionRolDto, GetByIdUsuario } from '../dto/index.dto';
import { AsignacionRepository } from '../repository/asignacion-rol.repository';

@Injectable()
export class AsignacionRolService {
  constructor(
    private readonly asignacionRepository: AsignacionRepository
  ){}

  create(createAsignacionRolDto: CreateAsignacionRolDto) {
    return this.asignacionRepository.crear(createAsignacionRolDto);
  }

  findAll() {
    return  this.asignacionRepository.listar();
  }

  findByUsuarioId(getByIdUsuario: GetByIdUsuario) {
    return this.asignacionRepository.listarByUsuario(getByIdUsuario)
  }

  remove(id: GetIdAsignacionRolDto) {
    return this.asignacionRepository.remover_asignacion(id);
  }
}
