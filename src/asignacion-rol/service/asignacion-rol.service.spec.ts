import { Test } from '@nestjs/testing';
import { AsignacionRolService } from './asignacion-rol.service';
import { AsignacionRepository } from '../repository/asignacion-rol.repository';
import { CreateAsignacionRolDto, GetIdAsignacionRolDto, GetByIdUsuario } from '../dto/index.dto';
import { UsuarioConRolesDTO } from '../interface/usuarioAsignacionRol.interface';
import { AsignacionRol } from '../entities/asignacion-rol.entity';

describe('AsignacionRolService', () => {
  let asignacionService: AsignacionRolService;
  let asignacionRepository: AsignacionRepository;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        AsignacionRolService,
        {
          provide: AsignacionRepository,
          useValue: {
            crear: jest.fn(),
            listar: jest.fn(),
            listarByUsuario: jest.fn(),
            remover_asignacion: jest.fn(),
          },
        },
      ],
    }).compile();

    asignacionService = moduleRef.get<AsignacionRolService>(AsignacionRolService);
    asignacionRepository = moduleRef.get<AsignacionRepository>(AsignacionRepository);
  });

  describe('create', () => {
    it('Deberia de crear una nueva asignacion', async () => {
      const createDto: CreateAsignacionRolDto = {
        usuario_id: 1,
        rol_id: 1,
      };

      const mockCreatedAssignment: AsignacionRol = new AsignacionRol({
        id: 1,
        usuario: null,
        rol: null,
        createAt: new Date(),
        updateAt: new Date(),
        deleteAt: null,
      });

      jest.spyOn(asignacionRepository, 'crear').mockResolvedValue(mockCreatedAssignment);

      const result = await asignacionService.create(createDto);

      expect(result).toEqual(mockCreatedAssignment);
      expect(asignacionRepository.crear).toHaveBeenCalledWith(createDto);
    });
  });

  describe('findAll', () => {
    it('Deberia de devolver a las asiganciones Usuario Rol', async () => {
      const mockAssignments: any[] = [
        {
          id: 1,
          usuario_id: 1,
          rol_id: 1,
        },
      ];

      jest.spyOn(asignacionRepository, 'listar').mockResolvedValue(mockAssignments);

      const result = await asignacionService.findAll();

      expect(result).toEqual(mockAssignments);
      expect(asignacionRepository.listar).toHaveBeenCalled();
    });
  });

  describe('findByUsuarioId', () => {
    it('Deberia de devolver a los roles de un usuario especifico', async () => {
      const getByIdUsuario: GetByIdUsuario = {
        id: 1,
      };

      const mockUserWithRoles: UsuarioConRolesDTO[] = [
        {
          id: 1,
          nombre: 'Test User',
          nombreUsuario: 'testuser',
          email: 'testuser@example.com',
          asignacionesRoles: [
            {
              id: 1,
              rol: {
                id: 1,
                nombre: 'Admin',
                descripcion: 'Administrator role',
              },
            },
            {
              id: 2,
              rol: {
                id: 2,
                nombre: 'RRHH',
                descripcion: 'Recursos Humanos role',
              },
            }
          ],
        },
      ];

      jest.spyOn(asignacionRepository, 'listarByUsuario').mockResolvedValue(mockUserWithRoles);

      const result = await asignacionService.findByUsuarioId(getByIdUsuario);

      expect(result).toEqual(mockUserWithRoles);
      expect(asignacionRepository.listarByUsuario).toHaveBeenCalledWith(getByIdUsuario);
    });
  });

  describe('remove', () => {
    it('Deberia eliminar la asignacion por ID', async () => {
      const getIdAsignacionRolDto: GetIdAsignacionRolDto = {
        id: 1,
      };

      jest.spyOn(asignacionRepository, 'remover_asignacion').mockResolvedValue(undefined);

      await asignacionService.remove(getIdAsignacionRolDto);

      expect(asignacionRepository.remover_asignacion).toHaveBeenCalledWith(getIdAsignacionRolDto);
    });
  });
});
