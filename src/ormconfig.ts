/* eslint-disable prettier/prettier */
import { DataSource, DataSourceOptions } from 'typeorm';
import { Usuario, Rol, AsignacionRol } from './entities/index';
import * as dotenv from 'dotenv';
dotenv.config();

export const dbdatasource: DataSourceOptions = {
  type: 'postgres',
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT),
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  entities: [Usuario, Rol, AsignacionRol],
  synchronize: (process.env.DB_SYNCHRONIZE.toLowerCase() === 'true') ? true : false,
  logging: true,
};

const dataSource = new DataSource(dbdatasource)
export default dataSource
