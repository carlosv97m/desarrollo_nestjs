import { Test, TestingModule } from '@nestjs/testing';
import { RolService } from './rol.service';
import { RolRepository } from '../repository/rol.repository';
import { CreateRolDto, UpdateRolDto, GetByIdDto } from '../dto/index.dto';
import { PaginationDto } from '../../common/dto/index.dto';

describe('RolService', () => {
  let service: RolService;
  let repository: RolRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RolService,
        {
          provide: RolRepository,
          useValue: {
            crear: jest.fn(),
            listar: jest.fn(),
            findById: jest.fn(),
            actualizar: jest.fn(),
            eliminar: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<RolService>(RolService);
    repository = module.get<RolRepository>(RolRepository);
  });

  it('Deberia de estar definido', () => {
    expect(service).toBeDefined();
  });

  describe('create', () => {
    it('Deberia de llamar al repositorio para guardar el nuevo Rol', async () => {
      const createRolDto: CreateRolDto = {
        nombre: 'administrador',
        descripcion: 'administrador del sistema',
      };
      await service.create(createRolDto);
      expect(repository.crear).toHaveBeenCalledWith(createRolDto);
    });
  });

  describe('findAll', () => {
    it('Deberia de llamar al repositorio para listar los registros de los Roles', async () => {
      const paginationDto: PaginationDto = { limit: 5, offset: 0, parameter: "" };
      await service.findAll(paginationDto);
      expect(repository.listar).toHaveBeenCalledWith(paginationDto);
    });
  });

  describe('findOne', () => {
    it('Deberia de llamar al repositorio para buscar a un registro del los Roles', async () => {
      const getByIdDto: GetByIdDto = { id: 1 };
      await service.findOne(getByIdDto);
      expect(repository.findById).toHaveBeenCalledWith(getByIdDto);
    });
  });

  describe('update', () => {
    it('Deberia de llamar al repositorio para actualizar un registro de Rol', async () => {
      const id = 1;
      const updateRolDto: UpdateRolDto = {
        id,
        nombre: 'administrador actualizado',
        descripcion: 'administrador del sistema Backend',
      };
      await service.update(id, updateRolDto);
      expect(repository.actualizar).toHaveBeenCalledWith(id, updateRolDto);
    });
  });

  describe('remove', () => {
    it('Deberia de llamar al repositorio para eliminar un registro de los Roles', async () => {
      const id = 1;
      await service.remove(id);
      expect(repository.eliminar).toHaveBeenCalledWith(id);
    });
  });
});
