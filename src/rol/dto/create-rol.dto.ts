import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";

export class CreateRolDto {
    @ApiProperty({
        example: 'administrador',
        required: true
    })
    @IsString()
    @IsNotEmpty()
    nombre: string;

    @ApiProperty({
        example: 'administrador del sistema',
        required: true
    })
    @IsString()
    @IsNotEmpty()
    descripcion: string;

}
