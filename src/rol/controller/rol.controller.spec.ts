import { Test, TestingModule } from '@nestjs/testing';
import { RolController } from './rol.controller';
import { RolService } from '../service/rol.service';
import { CreateRolDto, UpdateRolDto, GetByIdDto } from '../dto/index.dto';
import { PaginationDto } from '../../common/dto/index.dto';

describe('RolController', () => {
  let controller: RolController;
  let service: RolService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RolController],
      providers: [
        {
          provide: RolService,
          useValue: {
            create: jest.fn(),
            findAll: jest.fn(),
            findOne: jest.fn(),
            update: jest.fn(),
            remove: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<RolController>(RolController);
    service = module.get<RolService>(RolService);
  });

  it('Deberia de estar definido', () => {
    expect(controller).toBeDefined();
  });

  describe('create', () => {
    it('Debeia de llamar al servicio con los parametros esperados', async () => {
      const createRolDto: CreateRolDto = {
        nombre: 'administrador',
        descripcion: 'administrador del sistema',
      };
      await controller.create(createRolDto);
      expect(service.create).toHaveBeenCalledWith(createRolDto);
    });
  });

  describe('findAll', () => {
    it('DEberia llamar a listar con la paginacion de los datos', async () => {
      const paginationDto: PaginationDto = { limit: 5, offset: 0, parameter: '' };
      await controller.findAll(paginationDto);
      expect(service.findAll).toHaveBeenCalledWith(paginationDto);
    });
  });

  describe('findOne', () => {
    it('Deberia de llamar al servicio para buscar a solo un registro', async () => {
      const getByIdDto: GetByIdDto = { id: 1 };
      await controller.findOne(getByIdDto);
      expect(service.findOne).toHaveBeenCalledWith(getByIdDto);
    });
  });

  describe('update', () => {
    it('Deberia de llamar al servicio para modificar un registro de Rol', async () => {
      const id = 1;
      const updateRolDto: UpdateRolDto = {
        id: 1,
        nombre: 'administrador actualizado',
        descripcion: 'administrador del sistema Backend',
      };
      await controller.update(id.toString(), updateRolDto);
      expect(service.update).toHaveBeenCalledWith(id, updateRolDto);
    });
  });

  describe('remove', () => {
    it('Deberia llamar al servicio para eliminar el Rol', async () => {
      const id = 1;
      await controller.remove(id.toString());
      expect(service.remove).toHaveBeenCalledWith(id);
    });
  });

});
