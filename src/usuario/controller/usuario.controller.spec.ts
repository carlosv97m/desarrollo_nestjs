import { Test, TestingModule } from '@nestjs/testing';
import { UsuarioController } from './usuario.controller';
import { UsuarioService } from '../service/usuario.service';
import { CreateUsuarioDto } from '../dto/create-usuario.dto';
import { Usuario } from '../entities/usuario.entity';

describe('UsuarioController', () => {
  let controller: UsuarioController;
  let service: UsuarioService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsuarioController],
      providers: [
        {
          provide: UsuarioService,
          useValue: {
            create: jest.fn(),
            findAll: jest.fn().mockResolvedValue([
              new Usuario({ nombre: 'user1', nombreUsuario: 'user1', email: 'user1@test.com', password: 'password' }),
              new Usuario({ nombre: 'user2', nombreUsuario: 'user2', email: 'user2@test.com', password: 'password' }),
            ]),
          },
        },
      ],
    }).compile();

    controller = module.get<UsuarioController>(UsuarioController);
    service = module.get<UsuarioService>(UsuarioService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Deberia de crear un usuario', async () => {
    const createUsuarioDto: CreateUsuarioDto = {
      nombre: 'Carlos123',
      nombreUsuario: 'carlitos',
      email: 'carlitos@gmail.com',
      password: '12345678',
    };

    await controller.create(createUsuarioDto);

    expect(service.create).toHaveBeenCalledWith(createUsuarioDto);
  });

  it('Deberia de retornar a todos los usuarios', async () => {
    const paginationDto = { limit: 10, offset: 0 };

    const result = await controller.findAll(paginationDto);

    expect(result).toEqual([
      { nombre: 'user1', nombreUsuario: 'user1', email: 'user1@test.com', password: 'password' },
      { nombre: 'user2', nombreUsuario: 'user2', email: 'user2@test.com', password: 'password' },
    ]);
    expect(service.findAll).toHaveBeenCalledWith(paginationDto);
  });
});
