import { 
  Controller, 
  Get, 
  Post, 
  Body, 
  Patch, 
  Param, 
  Delete, 
  UseGuards,
  Request,
  Query} from '@nestjs/common';
import { UsuarioService } from '../service/usuario.service';
import { 
  CreateUsuarioDto, 
  UpdateUsuarioDto,
  VerificarDto,
  FindUsuarioDto } from '../dto/index.dto';
import { JwtStrategy, JwtauthGuard } from '../../auth/guards/index.guard';
import { ApiBody, ApiResponse, ApiTags } from '@nestjs/swagger';
import { PaginationDto } from '../../common/dto/index.dto';

@ApiTags('Usuario Controller')
// @UseGuards(JwtStrategy, JwtauthGuard)
@Controller('usuarios')
export class UsuarioController {
  constructor(private readonly usuarioService: UsuarioService) {}
  
  @ApiResponse({ status: 201, description: 'Se ha creado exitosamente' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 402, description: 'Error al registrar' })
  @ApiResponse({ status: 403, description: 'Usuario no encontrado' })
  @ApiBody({
    type: CreateUsuarioDto,
    description: 'Creando un nuevo usuario',
  })
  @Post()
  create(@Body() createUsuarioDto: CreateUsuarioDto) {
    return this.usuarioService.create(createUsuarioDto);
  }

  @ApiResponse({ status: 200, description: 'Usuario Encontrado' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 403, description: 'Usuario no encontrado' })
  @ApiBody({
    type: VerificarDto,
    description: 'Creando un nuevo usuario',
  })
  @Post('verificar')
  verificar(@Body() verificarDto: VerificarDto) {
    console.log("Revisar: ",verificarDto);
    return this.usuarioService.verificarCuenta(verificarDto);
  }
  
  @Get()
  findAll(@Query() paginationDto: PaginationDto) {
    return this.usuarioService.findAll(paginationDto);
  }

  @Get('/get-pokemon')
  getPokemon() {
    return this.usuarioService.getPokemon();
  }

  @Get('/get-pokemon/:id')
  getPokemonById(@Param('id') id: number ) {
    return this.usuarioService.getPokemonById(id);
  }
  
  @ApiResponse({ status: 402, description: 'Error al buscar' })
  @ApiResponse({ status: 403, description: 'Usuario no encontrado' })
  @ApiBody({
    type: FindUsuarioDto,
    description: 'Creando un nuevo usuario',
  })
  @Get(':id')
  findOne(@Body() findUsuarioDto: FindUsuarioDto) {
    return this.usuarioService.findOne(findUsuarioDto);
  }
  
  @ApiResponse({ status: 201, description: 'Se ha modificado exitosamente' })
  @ApiResponse({ status: 401, description: 'No autorizado' })
  @ApiResponse({ status: 402, description: 'Error al modificar' })
  @ApiResponse({ status: 403, description: 'Usuario no encontrado' })
  @ApiBody({
    type: UpdateUsuarioDto,
    description: 'Creando un nuevo usuario',
  })
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUsuarioDto: UpdateUsuarioDto) {
    return this.usuarioService.update(+id, updateUsuarioDto);
  }
  
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.usuarioService.remove(+id);
  }

}
