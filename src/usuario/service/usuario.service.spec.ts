import { Test, TestingModule } from '@nestjs/testing';
import { HttpService } from '@nestjs/axios';
import { UsuarioService } from './usuario.service';
import { UsuarioRepository } from '../repository/usuario.repository';
import { of, throwError } from 'rxjs';
import { InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { AxiosResponse, AxiosError, AxiosRequestHeaders } from 'axios';
import { CreateUsuarioDto, FindUsuarioDto, UpdateUsuarioDto, VerificarDto } from '../dto/index.dto';
import * as dotenv from 'dotenv';
dotenv.config();

const mockUsuarioRepository = {
  crear: jest.fn(),
  listar: jest.fn(),
  findById: jest.fn(),
  actualizar: jest.fn(),
  eliminar: jest.fn(),
  findByName: jest.fn(),
  findByEmail: jest.fn(),
};

const mockHttpService = {
  get: jest.fn(),
};

describe('UsuarioService', () => {
  let service: UsuarioService;
  let repository: UsuarioRepository;
  let httpService: HttpService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsuarioService,
        { provide: UsuarioRepository, useValue: mockUsuarioRepository },
        { provide: HttpService, useValue: mockHttpService },
      ],
    }).compile();

    service = module.get<UsuarioService>(UsuarioService);
    repository = module.get<UsuarioRepository>(UsuarioRepository);
    httpService = module.get<HttpService>(HttpService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('create', () => {
    it('Deberia de Agregar un nuevo usuario por el repositorio de Usuario', async () => {
      const createUsuarioDto: CreateUsuarioDto = {
        nombre: 'Carlos123',
        nombreUsuario: 'carlitos',
        email: 'carlitos@gmail.com',
        password: '12345678',
      };

      mockUsuarioRepository.findByName.mockResolvedValue(null);
      mockUsuarioRepository.crear.mockResolvedValue(createUsuarioDto);

      expect(await service.create(createUsuarioDto)).toEqual(createUsuarioDto);
      expect(mockUsuarioRepository.findByName).toHaveBeenCalledWith('Carlos123');
      expect(mockUsuarioRepository.crear).toHaveBeenCalledWith(createUsuarioDto);
    });

    it('Deberia de devolver un error si el usuario existe', async () => {
      const createUsuarioDto: CreateUsuarioDto = {
        nombre: 'Carlos123',
        nombreUsuario: 'carlitos',
        email: 'carlitos@gmail.com',
        password: '12345678',
      };

      mockUsuarioRepository.findByName.mockResolvedValue(createUsuarioDto);

      await expect(service.create(createUsuarioDto)).rejects.toThrow(InternalServerErrorException);
      expect(mockUsuarioRepository.findByName).toHaveBeenCalledWith('Carlos123');
    });
  });

  describe('findAll', () => {
    it('Deberia de listar a los usuario por el repositorio de Usuario', async () => {
      const paginationDto = { limit: 10, offset: 0 };
      const result = ['user1', 'user2'];
      mockUsuarioRepository.listar.mockResolvedValue(result);

      expect(await service.findAll(paginationDto)).toEqual(result);
      expect(mockUsuarioRepository.listar).toHaveBeenCalledWith(paginationDto);
    });
  });

  describe('findOne', () => {
    it('Derberia de retornar un usuario por busqueda por ID por el repositorio de Usuario', async () => {
      const findUsuarioDto: FindUsuarioDto = { id: 1 };
      const result = { id: 1, nombre: 'test' };
      mockUsuarioRepository.findById.mockResolvedValue(result);

      expect(await service.findOne(findUsuarioDto)).toEqual(result);
      expect(mockUsuarioRepository.findById).toHaveBeenCalledWith(1);
    });

    it('Deberia de retornar usuario no encontrado por ID', async () => {
      const findUsuarioDto: FindUsuarioDto = { id: 1 };
      mockUsuarioRepository.findById.mockResolvedValue(null);

      await expect(service.findOne(findUsuarioDto)).rejects.toThrow(NotFoundException);
      expect(mockUsuarioRepository.findById).toHaveBeenCalledWith(1);
    });
  });

  describe('update', () => {
    it('Deberia de actualizar un usuario por el repositorio de Usuario', async () => {
      const updateUsuarioDto: UpdateUsuarioDto = {
        id: 1,
        nombre: 'Carlos123',
        nombreUsuario: 'carlitos',
        email: 'carlitos@gmail.com',
        password: '12345678',
      };

      const result = { ...updateUsuarioDto, nombre: 'updated' };

      mockUsuarioRepository.findById.mockResolvedValue(result);
      mockUsuarioRepository.actualizar.mockResolvedValue(result);

      expect(await service.update(1, updateUsuarioDto)).toEqual(result);
      expect(mockUsuarioRepository.findById).toHaveBeenCalledWith(1);
      expect(mockUsuarioRepository.actualizar).toHaveBeenCalledWith(1, updateUsuarioDto);
    });

    it('Deberia de devolver un error al quere actualizar un usuario', async () => {
      mockUsuarioRepository.findById.mockResolvedValue(null);

      await expect(service.update(1, {} as any)).rejects.toThrow(NotFoundException);
      expect(mockUsuarioRepository.findById).toHaveBeenCalledWith(1);
    });
  });

  describe('remove', () => {
    it('Deberia de eliminar a un usuario', async () => {
      const result = { id: 1, nombre: 'test', deleteAt: new Date() };
      mockUsuarioRepository.findById.mockResolvedValue(result);
      mockUsuarioRepository.eliminar.mockResolvedValue(result);

      expect(await service.remove(1)).toEqual(result);
      expect(mockUsuarioRepository.eliminar).toHaveBeenCalledWith(1);
    });

    it('Deberia de devolver error al no encontrar el usuario', async () => {
      mockUsuarioRepository.findById.mockResolvedValue(null);

      await expect(service.remove(1)).rejects.toThrow(NotFoundException);
      expect(mockUsuarioRepository.findById).toHaveBeenCalledWith(1);
    });
  });

  describe('verificarCuenta', () => {
    it('Deberia de verificar el usuario y contraseña para validar el acceso', async () => {
      const verificarDto: VerificarDto = { email: 'test@test.com', password: 'test' };
      const result = { email: 'test@test.com', password: 'test' };
      mockUsuarioRepository.findByEmail.mockResolvedValue(result);

      expect(await service.verificarCuenta(verificarDto)).toEqual(result);
      expect(mockUsuarioRepository.findByEmail).toHaveBeenCalledWith('test@test.com');
    });

    it('Deberia de dar error al querer ingresar con creadenciales incorrectas', async () => {
      const verificarDto: VerificarDto = { email: 'test@test.com', password: 'wrong' };
      mockUsuarioRepository.findByEmail.mockResolvedValue({ email: 'test@test.com', password: 'test' });

      await expect(service.verificarCuenta(verificarDto)).rejects.toThrow(InternalServerErrorException);
    });
  });

  describe('getPokemon', () => {
    it('Deberia de mostrar informacion de la API', async () => {
      const headers = {
        'Content-Type': 'application/json'
      } as AxiosRequestHeaders;
  
      const result: AxiosResponse = {
        data: { results: ['pikachu'] },
        status: 200,
        statusText: 'OK',
        headers: headers,
        config: { headers: headers },
      };
  
      mockHttpService.get.mockReturnValue(of(result));
  
      const pokemonData = await service.getPokemon();
      expect(pokemonData).toEqual(result.data);
      expect(mockHttpService.get).toHaveBeenCalledWith(process.env.URL_API, { headers: headers });
    });
  
    it('Deberia de mostrar error al no llegar a la API', async () => {
      const error: AxiosError = new Error('API error') as AxiosError;
      mockHttpService.get.mockReturnValue(throwError(error));
  
      try {
        await service.getPokemon();
      } catch (e) {
        expect(e).toBeInstanceOf(InternalServerErrorException);
        expect(e.message).toEqual('API error');
      }
    });
  });
  

  describe('getPokemonById', () => {
    it('Deberia de devolver un pokemon por ID', async () => {
      const headers = {
        'Content-Type': 'application/json'
      } as AxiosRequestHeaders;
  
      const result: AxiosResponse = {
        data: { name: 'pikachu' },
        status: 200,
        statusText: 'OK',
        headers: headers,
        config: { headers: headers },
      };
  
      mockHttpService.get.mockReturnValue(of(result));
  
      const pokemonData = await service.getPokemonById(1);
      expect(pokemonData).toEqual(result.data);
      expect(mockHttpService.get).toHaveBeenCalledWith(`${process.env.URL_API}/${1}`, { headers: headers });
    });
  
    it('Deberia de dar un error por buscar un Pokemon por ID', async () => {
      const error: AxiosError = new Error('API error') as AxiosError;
      mockHttpService.get.mockReturnValue(throwError(error));
  
      try {
        await service.getPokemonById(1);
      } catch (e) {
        expect(e).toBeInstanceOf(InternalServerErrorException);
        expect(e.message).toEqual('API error');
      }
    });
  });
  
});
